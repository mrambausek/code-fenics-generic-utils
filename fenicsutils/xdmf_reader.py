from abc import abstractmethod
import typing
import numpy as np
import pathlib
import itertools
import h5py
import xml.etree.ElementTree as ET
import copy


def time_fmt(time: typing.Union[float, str]):
    return "{:.14e}".format(float(time))


def print_element(elmt, level=0):
    print("\t"*level, "tag: {:s} -- attributes: {!s}, text: {!s}".format(elmt.tag, elmt.attrib, elmt.text))
    for subelmt in elmt:
        print_element(subelmt, level+1)


def get_elements(root_element, predicate, depth=1):
    """Use neg depth for 'infinite' recursion"""
    if predicate(root_element):
        return [root_element]
    elif depth != 0:
        return sum([get_elements(elmt, predicate, depth-1) for elmt in root_element], [])
    else:
        return []


def get_all_time_stamps(root_element):
    timetag = [elmt for elmt in root_element if elmt.tag == "Time"]
    if len(timetag) == 1:
        return [timetag[0].attrib["Value"]]
    elif len(timetag) > 1:
        raise RuntimeError("Two time tags per element are unexpected")
    else:
        return sum([get_all_time_stamps(elmt) for elmt in root_element], [])


def get_all_time_series(root_element):
    return get_elements(root_element, lambda item: item.attrib.get("Name", None) == "TimeSeries", depth=-1)


def get_time_data(root_element, time: typing.Union[float, str], tol=1e-14):
    timetag = [elmt for elmt in root_element if elmt.tag == "Time"]
    if len(timetag) == 1 and np.abs(float(timetag[0].attrib["Value"]) - float(time)) / (float(time) + 1) < tol:
        return root_element
    elif len(timetag) > 1:
        raise RuntimeError("Two time tags per element are unexpected")
    else:
        found = [get_time_data(elmt, time) for elmt in root_element]
        found = [elmt for elmt in found if elmt is not None]
        return found[0] if len(found) > 0 else None


def get_data_item(time_data, predicate, hf=None, data_dir=pathlib.Path(), depth=1):
    field = get_elements(time_data, predicate, depth=depth)
    if len(field) == 0:
        return None
    elif len(field) > 1:
        raise RuntimeError("Found more than one field!")
    else:
        field = field[0]
    link = get_elements(field, 
                        lambda item: item.tag == "DataItem" and item.attrib.get("Format") == "HDF", 
                        depth=1)[0]
    fname, hfield = link.text.split(":")
    
    def _return(hf):
        return (dict(field.attrib), dict(link.attrib), dict(hf[hfield].attrs)), np.array(hf[hfield])
    
    if hf is not None:
        return _return(hf)
    else:
        with h5py.File(data_dir / fname, mode="r") as hf:
            return _return(hf)


def points_in_box(points, coords, dd):
    return np.product(np.array([np.abs(points[:,ii] - coord) < dd/2 for ii, coord in enumerate(coords)]), axis=0)


def get_node_indices(points, coords, dd):
    return np.where(points_in_box(points, coords, dd))[0]


def get_element_indices(elements, node_indices):
    return np.where(sum([elements == node_index for node_index in node_indices]))[0]


def select_points(points, coords, dd):
    _indices = get_node_indices(points, coords, dd)
    return list(zip(_indices, points[_indices]))


def select_elements(elements, points, coords, dd):
    _indices = get_element_indices(elements, get_node_indices(points, coords, dd))
    return list(zip(_indices, elements[_indices]))


def field_selector_by_name(name, entries: typing.Iterable[int]=None, data_dir=pathlib.Path()):
    def _func(time_data):
        data = get_data_item(time_data, lambda item: item.attrib.get("Name") == name, data_dir=data_dir)
        if data is None:
            return name, {elmt: None for elmt in entries} if entries is not None else None
            
        _entries = {elmt: data[-1][elmt] for elmt in entries} if entries is not None else data[-1]
        return name, _entries
    return _func


def selection_over_time(time_series: ET.Element, 
                        data_selectors: typing.Iterable[typing.Callable[[ET.Element], typing.Dict[str, np.ndarray]]],
                        time_stamps: typing.Iterable[typing.Union[float, str]]=None):
    time_stamps = get_all_time_stamps(time_series) if time_stamps is None else time_stamps
    selected = {}
    for time_stamp in time_stamps:
        time_data = get_time_data(time_series, time_stamp)
        selected[time_fmt(time_stamp)] = dict(selector(time_data) for selector in data_selectors)
    return selected


class SelectionOverTime:

    def __init__(self, time_series: ET.Element, 
        data_selectors: typing.Iterable[typing.Callable[[ET.Element], typing.Dict[str, np.ndarray]]],
        time_stamps: typing.Iterable[typing.Union[float, str]]=None):
        self._data = selection_over_time(time_series, data_selectors, time_stamps=time_stamps)
        
    @property
    def data(self):
        return self._data
    
    def time(self):
        return np.array(sorted([float(kk) for kk in self.data.keys()]))
        
    def field(self, name: str, entry: int=None):
        for val in self.data.values():
            break
        if name not in val.keys():
            return None
        if entry is None:
            if isinstance(val[name], dict):
                return {kk: np.array([value[name][kk] for value in self.data.values()])
                        for kk in val[name].keys()}
            else:
                return np.array([value[name] for value in self.data.values()]).swapaxes(0, 1)
        else:
            return np.array([value[name][entry] for value in self.data.values()])


class XDMFReader:

    def __init__(self, xfpath: typing.Union[pathlib.Path, str]):
        self._fpath = pathlib.Path(xfpath).resolve()
        self._data_dir = self._fpath.parent
        self._time_series = None
        self._time_stamps = None
        self._geometries = None
        self._topologies = None
        with open(self._fpath, 'r') as xf:
            self.metadata = ET.parse(xf).getroot()
            
    @property
    def fpath(self):
        return self._fpath
        
    @property
    def data_dir(self):
        return self._data_dir

    @property
    def time_series(self):
        if self._time_series is None:
            all_time_series = get_all_time_series(self.metadata)
            if len(all_time_series) == 0:
                raise RuntimeError("Failed to find time series")
            elif len(all_time_series) == 1:
                self._time_series = all_time_series[0]
            else:
                raise RuntimeError("Found more than one time series. This is not supported.")
        return self._time_series
    
    @property
    def time_stamps(self):
        if self._time_stamps is None:
            self._time_stamps = get_all_time_stamps(self.time_series)
        return self._time_stamps
        
    @property
    def geometries(self):
        def _select(time_data):
            data = get_data_item(time_data, lambda item: item.tag == "Geometry", depth=1, data_dir=self._data_dir)
            return "Geometry", (data[-1] if data is not None else None)
            
        if self._geometries is None:
            geometries = selection_over_time(self.time_series, [_select], time_stamps=self.time_stamps)
            self._geometries = {kk: vv["Geometry"] for kk, vv in geometries.items() if vv["Geometry"] is not None}
        return self._geometries
    
    @property
    def topologies(self):
        def _select(time_data):
            data = get_data_item(time_data, lambda item: item.tag == "Topology", depth=1, data_dir=self._data_dir)
            return "Topology", (data[-1] if data is not None else None)
            
        if self._topologies is None:
            topologies = selection_over_time(self.time_series, [_select], time_stamps=self.time_stamps)
            self._topologies = {kk: vv["Topology"] for kk, vv in topologies.items() if vv["Topology"] is not None}
        return self._topologies
        
    def field_selector_by_name(self, name, entries: typing.Iterable[int]=None):
        return field_selector_by_name(name, entries=entries, data_dir=self._data_dir)
        
    def selection_over_time(self, data_selectors: typing.Iterable[typing.Callable[[ET.Element], typing.Dict[str, np.ndarray]]], 
                            time_stamps: typing.Iterable[typing.Union[float, str]]=None):
        return SelectionOverTime(self.time_series, data_selectors, time_stamps=time_stamps)
