

import os
from typing import Iterable
import dolfin.fem.form
import dolfin.jit.jit
from dolfin.cpp.parameter import parameters as dolfin_parameters
import ufl
import ffc


# Wrap FFC JIT compilation with decorator
@dolfin.jit.jit.mpi_jit_decorator
def compile_ufl_object(ufl_object, module_name: str, form_compiler_parameters: dict=None, clean=False):
    """
    Compile a ufl object to a module with given name.
    :param ufl_object: The object to be compiled. UFL Form, FE or DofMap
    :param module_name: name of the module. Note that caching by default prevent recompilation!
    :param form_compiler_parameters: dict with standard dolfin/FFC form compiler parameters
    :param clean: Whether the module directory should be removed before compliation. Triggers rebuild.
    :return: the compiled module
    """
    # Prepare form compiler parameters with overrides from dolfin and kwargs
    if clean:
        cache_dir = form_compiler_parameters.get('cache_dir', os.environ("DIJITSO_CACHE_DIR"))
        if os.path.exists(cache_dir) and os.path.isdir(cache_dir):
            os.system('rm -rf {:s}'.format(cache_dir))

    p = ffc.default_jit_parameters()
    p.update(dict(dolfin_parameters["form_compiler"]))
    p.update(form_compiler_parameters or {})
    return ffc.jitcompiler.jit_build(ufl_object, module_name, p)


def compile_ufl_forms(forms: Iterable[ufl.Form], form_compiler_parameters: dict=None):
    """
    Create compiled dolfin forms from ufl forms. Can be used to trigger compilation without assembly.
    :param forms: ufl forms to compile
    :param form_compiler_parameters:
    :return: List of dolfin Forms
    """
    def _make_form(form, form_compiler_parameters):
        if isinstance(form, ufl.Form):
            return dolfin.fem.form.Form(
                form, form_compiler_parameters=form_compiler_parameters)
        else:
            raise TypeError("Invalid form type %s" % (type(form),))

    return [_make_form(form, form_compiler_parameters) for form in forms]
