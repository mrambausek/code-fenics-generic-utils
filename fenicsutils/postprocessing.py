from abc import ABC, abstractmethod
from typing import List
import typing
import numpy as np
import pathlib
import itertools
import h5py
import xml.etree.ElementTree as ET
import copy
import ufl.core.expr
import dolfin

from .common import get_function_space


def get_dP_element(expression, cell_name: str, order: int = 0):
    shape = expression.ufl_shape
    element_type = "DP"
    if len(shape) == 0:
        return dolfin.FiniteElement(element_type, cell_name, order)
    else:
        return dolfin.TensorElement(element_type, cell_name, order, shape=shape)


def _mul(v, expr):
    if len(v.ufl_shape) == 0:
        return v * expr
    else:
        return dolfin.inner(v, expr)


def XDMFFile(name: str, *args, functions_share_mesh=True, rewrite_function_mesh=False, flush_output=True):
    """Convenience wrapper with good default settings for dolfin.XDMFFile"""
    xf = dolfin.XDMFFile(name, *args)
    xf.parameters["functions_share_mesh"] = functions_share_mesh
    xf.parameters["rewrite_function_mesh"] = rewrite_function_mesh
    xf.parameters["flush_output"] = flush_output
    return xf


class Postprocessor(ABC):
    """
    Abstract base class
    """

    @abstractmethod
    def field(self) -> dolfin.Function:
        pass


def write_to_file(postprocessors: List, ofile: dolfin.XDMFFile, time=0.0):
    """
    Batch-processes postprocessors. Writes their fields to given file
    """
    for postprocessor in postprocessors:
        ofile.write(postprocessor.field(), float(time))


class InterpolationPostprocessor(Postprocessor):
    """
    Postprocessor that takes a function and internally creates either a contiuous or a discontinous 
    interpolation of 'solution'.
    """

    def __init__(self, solution: dolfin.Function, name: str=None, continuous: bool=True, order: int=1):
        self._solution = solution
        name = solution.name() if name is None else name
        mesh = solution.function_space().mesh()
        element = solution.function_space().ufl_element()
        V = get_function_space(mesh, element)
        self._hfunc = dolfin.Function(V)

        family = "P" if continuous else "DP"
        if len(element.value_shape()) == 0:
            pp_element = dolfin.FiniteElement(family, mesh.cell_name(), order)
        else:
            pp_element = dolfin.TensorElement(family, mesh.cell_name(), order, shape=element.value_shape())
        self._pp_field = dolfin.Function(get_function_space(mesh, pp_element), name=name)

    def field(self):
        dolfin.assign(self._hfunc, self._solution)
        self._pp_field.interpolate(self._hfunc)
        return self._pp_field


class LocalProjectionPostprocessor(Postprocessor):
    """
    Postprocessor that computes an element-local projection of given expressions.
    """

    @staticmethod
    def get_function_space(mesh, expressions, order: int = 0):
        try:
            expression = list(expressions.values())[0]()
        except (TypeError, AttributeError):
            expression = expressions()
        element = get_dP_element(expression, mesh.cell_name(), order)
        return get_function_space(mesh, element)

    @staticmethod
    def create_bilinear_form(V):
        v = dolfin.TestFunction(V)
        u = dolfin.TrialFunction(V)
        dx = dolfin.Measure("dx", domain=V.mesh())
        return _mul(v, u) * dx

    @staticmethod
    def create_linear_form(expressions, V, dx):
        v = dolfin.TestFunction(V)
        try:
            return sum([_mul(v, expression()) * dx(ii) for ii, expression in expressions.items()])
        except (TypeError, AttributeError):
            return sum([_mul(v, expressions()) * dx])

    @staticmethod
    def create_local_solver(expressions, mesh, subdomains, order, quadrature_degree):
        V = LocalProjectionPostprocessor.get_function_space(mesh, expressions, order=order)
        dx = dolfin.Measure("dx", domain=mesh, subdomain_data=subdomains,
                            metadata={"quadrature_degree": quadrature_degree})
        a = LocalProjectionPostprocessor.create_bilinear_form(V)
        L = LocalProjectionPostprocessor.create_linear_form(expressions, V, dx)
        return dolfin.LocalSolver(a, L)

    @staticmethod
    def create_pp_field(expressions, mesh, function_name):
        V = LocalProjectionPostprocessor.get_function_space(mesh, expressions)
        return dolfin.Function(V, name=function_name)

    @staticmethod
    def make_from_material_dict(function_name: str, 
                                material_dict: typing.Dict[int, typing.Dict[str, typing.Callable[[], ufl.core.expr.Expr]]], 
                                mesh: dolfin.Mesh, subdomains: dolfin.MeshFunction, quadrature_degree: int=4, order: int=0, 
                                attr_name: str=None):
        """
        Factory
        
        Parameters 
        ----------
        
        function_name
            The name of the function in postprocessing
        
        material_dict
            Either a dict where the keys are domain indicators and the values are dict where the key indicate the field that
            the value (callabel return an expression) corresponds to.
        
        mesh
            The mesh
            
        subdomains
            The subdomains indicating integration domains. Must fit the keys in expressions.
            
        quadrature_degree
            The quadrature degree with which the projection is performed
            
        order
            The local polynomial order of the resulting discontiuous Lagrange function
            
        attr_name
            Used to select the desired field from material_dict, i.e. material_dict[ii][attr_name].
            Defaults to function_name.
        """
        attr_name = function_name if attr_name is None else attr_name

        def _get_attributes(object_dict, attr_name):
            return dict([(kk, getattr(vv, attr_name)) for kk, vv in object_dict.items()])

        return LocalProjectionPostprocessor(_get_attributes(material_dict, attr_name), mesh, function_name,
                                            subdomains=subdomains, quadrature_degree=quadrature_degree, order=order)

    def __init__(self, expressions: typing.Union[typing.Callable[[], ufl.core.expr.Expr], 
                                                 typing.Dict[int, typing.Callable[[], ufl.core.expr.Expr]]], 
                 mesh: dolfin.Mesh, function_name: str, subdomains: dolfin.MeshFunction=None, quadrature_degree: int=4, order: int=0):
        """
        Constructor
        
        Parameters 
        ----------
        
        expressions
            Either a dict of callables or a single callable return an expression. In the first case, the returned 
            expressions must be of same the same shape.
        
        mesh
            The mesh
        
        function_name
            The name of the function in postprocessing
            
        subdomains
            The subdomains indicating integration domains. Must fit the keys in expressions.
            
        quadrature_degree
            The quadrature degree with which the projection is performed
            
        order
            The local polynomial order of the resulting discontiuous Lagrange function
        """
        self._local_solver = self.create_local_solver(expressions, mesh, subdomains, order, quadrature_degree)
        self._pp_field = self.create_pp_field(expressions, mesh, function_name)

    def field(self):
        self._local_solver.solve_global_rhs(self._pp_field)
        return self._pp_field


def make_local_projection_postprocessor_from_dict(
        function_name: str, material_dict: typing.Dict[int, typing.Dict[str, typing.Callable[[], ufl.core.expr.Expr]]], 
        mesh: dolfin.Mesh, subdomains: dolfin.MeshFunction, quadrature_degree: int=4, order: int=0):
    """
        Factory for LocalProjectionPostprocessor
        
        Parameters 
        ----------
        
        function_name
            The name of the function in postprocessing. At the same time, the given name is used to select the desired 
            field from the material_dict, i.e. material_dict[ii][function_name].
        
        material_dict
            Either a dict where the keys are domain indicators and the values are dict where the key indicate the field that
            the value (callabel return an expression) corresponds to.
        
        mesh
            The mesh
            
        subdomains
            The subdomains indicating integration domains. Must fit the keys in expressions.
            
        quadrature_degree
            The quadrature degree with which the projection is performed
            
        order
            The local polynomial order of the resulting discontiuous Lagrange function
        """
    return LocalProjectionPostprocessor.make_from_material_dict(function_name, material_dict, mesh, subdomains,
                                                                quadrature_degree=quadrature_degree, order=order)


def make_local_projection_postprocessors_from_dict(
        function_names: typing.Iterable[str], material_dict: typing.Dict[int, typing.Dict[str, typing.Callable[[], ufl.core.expr.Expr]]], 
        mesh: dolfin.Mesh, subdomains: dolfin.MeshFunction, quadrature_degree: int=4, order: int=0):
    """
    Factory for LocalProjectionPostprocessor
    
    Parameters 
    ----------
    
    function_names
        The name of the functions in postprocessing. At the same time, the given names are used to select the desired 
        fields from the material_dict, i.e. material_dict[ii][function_name].
    
    material_dict
        Either a dict where the keys are domain indicators and the values are dict where the key indicate the field that
        the value (callabel return an expression) corresponds to.
    
    mesh
        The mesh
        
    subdomains
        The subdomains indicating integration domains. Must fit the keys in expressions.
        
    quadrature_degree
        The quadrature degree with which the projection is performed
        
    order
        The local polynomial order of the resulting discontiuous Lagrange function
    """
    return [make_local_projection_postprocessor_from_dict(function_name, material_dict, mesh, subdomains,
                                                          quadrature_degree=quadrature_degree, order=order)
            for function_name in function_names]
