from dolfin import FunctionSpace


class ConstantCache(object):
    constants = {}


class FSCache(object):
    storage = {}


def get_function_space(mesh, element):
    _function_space_cache = FSCache.storage
    key = "{:d}|{:s}".format(id(mesh), repr(element))
    if key not in _function_space_cache.keys():
        _function_space_cache[key] = FunctionSpace(mesh, element)
    return _function_space_cache[key]


def make_scalar_constant(name, value):
    if name in ConstantCache.constants.keys():
        print("No new constant generated, because there already exists a constant with this name.")
    else:
        cmd = "Expression(\"{name:s}\", {name:s}={value:.8e}, degree=0)".format(name=name, value=value)
        print("evaluating '{:s}'".format(cmd))
        expr_ = eval(cmd)
        ConstantCache.constants[name] = expr_
    return ConstantCache.constants[name]
