from typing import Iterable
from dolfin import Function, Form, derivative, DirichletBC, solve, assemble
from ufl import replace


class Sensitivity(object):
    """
    Adjoint sensitivity analysis for path-independent quantities/problems.
    """
    def __init__(self, solution: Function, qoi_form: Form, constraint_form: Form,
                 homogeneous_dirichlet_bcs: Iterable[DirichletBC]=None,
                 linear_solver_options: Iterable[str]=('mumps',)):
        v = constraint_form.arguments()[0]
        self._lagrange_multipliers = Function(solution.function_space())
        self._constraint_form = constraint_form
        self._L = {"qoi_form": qoi_form, "constraint_form": -replace(constraint_form, {v: self._lagrange_multipliers})}
        self._dL = dict([(k, derivative(v, solution)) for k, v in self._L.items()])
        self._ddL = dict([(k, derivative(v, self._lagrange_multipliers)) for k, v in self._dL.items()])
        self._adj_rhs = None
        self._adj_lhs = None

        self._linear_solver_options = linear_solver_options

        self._bcs = homogeneous_dirichlet_bcs if homogeneous_dirichlet_bcs is not None else []
        self._indep_func = None
        self._sens_forms = None
        self._sens = None

    def _assemble_adjoint_rhs(self):
        if self._adj_rhs is None:
            self._adj_rhs = assemble(-self._dL["qoi_form"])
        else:
            assemble(-self._dL["qoi_form"], tensor=self._adj_rhs)
        # this can be omitted since lagrange multipliers are zero at this point
        # assemble(-self._dL["constraint_form"], tensor=self._adj_rhs, add_values=True)

    def _assemble_adjoint_lhs(self):
        # note that self._ddL["qoi_form"] is always zero!
        if self._adj_lhs is None:
            self._adj_lhs = assemble(self._ddL["constraint_form"])
        else:
            assemble(self._ddL["constraint_form"], tensor=self._adj_lhs)

    def _compute_adjoint_sensitivity(self):
        self._lagrange_multipliers.vector().zero()
        self._assemble_adjoint_rhs()
        self._assemble_adjoint_lhs()
        for _bc in self._bcs:
            _bc.apply(self._adj_lhs, self._adj_rhs)
        solve(self._adj_lhs, self._lagrange_multipliers.vector(), self._adj_rhs, *self._linear_solver_options)

    def _assemble_sensitivity(self):
        if self._sens is None:
            self._sens = assemble(self._sens_forms["qoi_form"])
        else:
            assemble(self._sens_forms["qoi_form"], tensor=self._sens)
        assemble(self._sens_forms["constraint_form"], tensor=self._sens, add_values=True)

    def compute(self, independent_func: Function):
        self._compute_adjoint_sensitivity()
        if independent_func != self._indep_func:
            self._sens_forms = dict([(k, derivative(v, independent_func)) for k, v in self._L.items()])
            self._indep_func = independent_func
        self._assemble_sensitivity()
        return self._sens

    def get_stored_sensitivity(self):
        return self._sens
