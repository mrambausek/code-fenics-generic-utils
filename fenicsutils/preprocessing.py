from typing import Callable, Iterable, Dict, Set, List
import numpy as np
import dolfin
from dolfin import Cell, Facet, FunctionSpace, DofMap, cells
from dolfin.cpp.mesh import faces


def get_global_indices(dofmap: DofMap, local_indices: Iterable[int]):
    return set(dofmap.local_to_global_index(local_index) for local_index in local_indices)


def get_locally_owned_indices(dofmap: DofMap, global_indices: Iterable[int]):
    return set(global_indices) - set(dofmap.local_to_global_unowned())


def find_local_dofs(function_space: FunctionSpace,
                    domain_filter: Callable[[Cell], bool] = lambda item: True,
                    coordinate_filter: Callable[[np.ndarray], bool] = lambda item: True)\
        -> Dict[int, np.ndarray]:
    """
    Find dofs based on domains and coordinates.
    :param function_space:
    :param domain_filter:
    :param coordinate_filter:
    :return: dict {local_index: coords}
    """
    mesh = function_space.mesh()
    dofmap = function_space.dofmap()
    element = function_space.element()
    found_dofs = {}
    for cell in filter(domain_filter, cells(mesh)):
        cell_coords = element.tabulate_dof_coordinates(cell)
        found_entries = np.array([idx for idx, coords in enumerate(cell_coords) if coordinate_filter(coords)])
        cell_dofs = dofmap.cell_dofs(cell.index())
        for idx in found_entries:
            found_dofs[cell_dofs[idx]] = cell_coords[idx]
    return found_dofs


def find_local_dofs_with_form(dummy_form: dolfin.Form) -> np.ndarray:
    """
    This is a quite flexible tool to find (local) DOF indices. For example, to find all dofs affecting the
    the fields within a certain subdomain, use a form like
    ```
    eta = TestFunction(V)
    dot(Constant(np.ones(eta.ufl_shape())), eta) * dx(subdomain_index)
    ```
    :param dummy_form:
    :return:
    """
    dummy_load = dolfin.assemble(dummy_form)
    return np.where(dummy_load.get_local() != 0)[0]


def find_cells_for_dof_index(dof_index: int, function_space: FunctionSpace,
                             domain_filter: Callable[[Cell], bool] = lambda item: True) -> Set[dolfin.Cell]:
    mesh = function_space.mesh()
    dofmap = function_space.dofmap()
    found_cells = set()
    for cell in filter(domain_filter, cells(mesh)):
        if dof_index in dofmap.cell_dofs(cell.index()):
            found_cells.add(cell.index())
    return found_cells


def find_facet_for_dof_index(dof_index: int, function_space: FunctionSpace,
                             domain_filter: Callable[[Facet], bool] = lambda item: True) -> Set[dolfin.Facet]:
    mesh = function_space.mesh()
    dofmap = function_space.dofmap()
    found_facets= set()
    for facet in filter(domain_filter, dolfin.cpp.mesh.facets(mesh)): # dolfin.cpp.mesh.
        if dof_index in dofmap.cell_dofs(facet.index()):
            found_facets.add(facet.index())
    return found_facets


def get_cells_of_point(mesh: dolfin.Mesh, point: np.ndarray) -> List[int]:
    return mesh.bounding_box_tree().compute_entity_collisions(dolfin.Point(*point))


def get_closest_cell_of_point(mesh: dolfin.Mesh, point: np.ndarray) -> List[int]:
    return mesh.bounding_box_tree().compute_closest_entity(dolfin.Point(*point))[0]


def get_subdomains_of_cells(subdomains: dolfin.MeshFunction, cell_indices: Iterable[int]) -> Set[int]:
    return set([subdomains[cell_idx] for cell_idx in cell_indices])


def get_subdomains_of_point(mesh: dolfin.Mesh, subdomains: dolfin.MeshFunction, point: np.ndarray) -> Set[int]:
    """
    Warning: This function sometimes suffers from too stric tolerances for collision detection. Points on interfaces
    may be attributed to only one side!
    :param mesh:
    :param subdomains:
    :param point:
    :return:
    """
    subdomains_set = get_subdomains_of_cells(subdomains, get_cells_of_point(mesh, point))
    if not bool(subdomains_set):
        subdomains_set = get_subdomains_of_cells(subdomains, [get_closest_cell_of_point(mesh, point)])
    return subdomains_set
