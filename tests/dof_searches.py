

from dolfin import *
import numpy as np
import os


print(MPI.rank(MPI.comm_world), MPI.size(MPI.comm_world))


class configuration():
    hdiv_elem = "N2F"
    alpha = np.pi/6


# ## Read the mesh
mesh = Mesh(MPI.comm_world)

with XDMFFile("meshes/mesh.xdmf") as xf:
    xf.read(mesh)
    subdomains = MeshFunction("size_t", mesh, 3)
    xf.read(subdomains, "gmsh:physical")

FREE_SPACE = 0
SPECIMEN = 1
ELECTRODE = 2

# Setup the quadrature
dx = dx(domain=mesh, subdomain_data=subdomains, metadata={"quadrature_degree": 5})

# Function Space and functions


# test bcs
V_bc = FunctionSpace(mesh, "CG", 2)



# ==============
# THE MANUAL WAY
# ==============


tol = 1e-3
L_s = 2.0
H_s = 2.0
R_s = 0.05
H_e = 0.05
R_e = 0.025
L_e = L_s - 2 * R_s


## Volume domains


def in_electrode(x, on_boundary):
    R_12_sq = x[0] ** 2 + x[1] ** 2
    L_ei = L_e / 2 - R_e
    H_ei = (H_s / 2 + H_e) - R_e
    return (H_s / 2 + H_e) + tol > abs(x[2]) > H_s / 2 - tol and R_12_sq - (L_e / 2) ** 2 < tol**2 and (
            abs(x[2]) - H_ei < tol or R_12_sq - L_ei**2 < tol or
            (R_12_sq - (L_ei + np.sqrt(R_e ** 2 - (H_ei - abs(x[2])) ** 2)) ** 2 < tol**2)
    )


def in_specimen(x, on_boundary):
    R_12_sq = x[0] ** 2 + x[1] ** 2
    L_si = L_s / 2 - R_s
    H_si = H_s / 2 - R_s
    return abs(x[2]) - H_s / 2 < tol and R_12_sq - (L_s / 2) ** 2 < tol**2 and (
            abs(x[2]) - H_si < tol or R_12_sq - L_si**2 < tol or
            (R_12_sq - (L_si + np.sqrt(R_s ** 2 - (H_si - abs(x[2])) ** 2)) ** 2 < tol**2)
    )


def in_bodies(x, on_boundary):
    return in_specimen(x, on_boundary) or in_electrode(x, on_boundary)


def in_free_space(x, on_boundary):
    return not in_bodies(x, on_boundary)


class VolumeDomain(SubDomain):
    def __init__(self, func):
        super().__init__()
        self._func = func

    def inside(self, x, on_boundary):
        return self._func(x, on_boundary)


specimen_domain = VolumeDomain(in_specimen)
electrode_domain = VolumeDomain(in_electrode)
bodies_domain = VolumeDomain(in_bodies)
free_space_bc_domain = VolumeDomain(in_free_space)


# ### Boundary domains

def free_space_boundary(x, on_boundary):
    return on_boundary and near(np.sqrt(np.dot(x, x)), 20, 1e-1)


def vertical_symmetry_boundary(x, on_boundary):
    return near(x[2], 0, tol)


def vertical_symmetry_boundary_bodies(x, on_boundary):
    return vertical_symmetry_boundary(x, on_boundary) and x[0] ** 2 + x[1] ** 2 - (L_s / 2) ** 2 < tol ** 2


def vertical_symmetry_boundary_free_space(x, on_boundary):
    return vertical_symmetry_boundary(x, on_boundary) and x[0] ** 2 + x[1] ** 2 - (L_s / 2) ** 2 > tol ** 2


def rotation_axis(x, on_boundary):
    return x[0] < tol and x[1] < tol


def rotation_axis_bodies(x, on_boundary):
    return rotation_axis(x, on_boundary) and x[2] < H_s / 2 + H_e + tol


def rotation_axis_free_space(x, on_boundary):
    return rotation_axis(x, on_boundary) and x[2] > H_s / 2 + H_e + tol


def rotational_symmetry_boundary(x, on_boundary):
    return x[1] < tol or near(x[1] / x[0], np.tan(configuration.alpha), tol)


def l_domain_bodies(x, on_boundary):
    return in_bodies(x, on_boundary) and rotational_symmetry_boundary(x, on_boundary)


def l_domain_free_space(x, on_boundary):
    return in_free_space(x, on_boundary) and rotational_symmetry_boundary(x, on_boundary)


base_names = [item.__name__ for item in (vertical_symmetry_boundary_bodies,
                                         vertical_symmetry_boundary_free_space,
                                         rotation_axis_bodies,
                                         rotation_axis_free_space,
                                         l_domain_bodies,
                                         l_domain_free_space)]


# ## Boundary Conditions

# ### Strong Dirichlet Boundary Conditions

bcs = [DirichletBC(V_bc, Constant(1), globals()[item], method="pointwise") for item in base_names]


# ==================
# THE SMARTER WAY(?)
# ==================

from fenicsutils import preprocessing


def get_subdomains_of_point(x):
    return preprocessing.get_subdomains_of_point(mesh, subdomains, x)


bodies = {SPECIMEN, ELECTRODE}
free_space = {FREE_SPACE}


def in_bodies_new(x, onb):
    return not get_subdomains_of_point(x).isdisjoint(bodies)


def in_free_space_new(x, onb):
    return get_subdomains_of_point(x) == free_space


def rotation_axis_bodies_new(x, onb):
    return rotation_axis(x, onb) and in_bodies_new(x, onb)


def rotation_axis_free_space_new(x, onb):
    return rotation_axis(x, onb) and in_free_space_new(x, onb)


def l_domain_bodies_new(x, onb):
    return rotational_symmetry_boundary(x, onb) and in_bodies_new(x, onb)


def l_domain_free_space_new(x, onb):
    return rotational_symmetry_boundary(x, onb) and in_free_space_new(x, onb)


def vertical_symmetry_boundary_bodies_new(x, onb):
    return vertical_symmetry_boundary(x, onb) and in_bodies_new(x, onb)


def vertical_symmetry_boundary_free_space_new(x, onb):
    return vertical_symmetry_boundary(x, onb) and in_free_space_new(x, onb)


# ## Boundary Conditions

# ### Strong Dirichlet Boundary Conditions

bcs_new = [DirichletBC(V_bc, Constant(1), globals()[item+"_new"], method="pointwise") for item in base_names]


# run the tests

bc_func_1 = Function(V_bc, name="bc_func")
bc_func_2 = Function(V_bc, name="bc_func")

aa_bc = TestFunction(V_bc) * TrialFunction(V_bc) * dx
L_bc = Constant(0) * TestFunction(V_bc) * dx


def test_bc(bc1, bc2, ii):
    bc_func_1.vector().zero()
    bc_func_2.vector().zero()
    solve(aa_bc == L_bc, bc_func_1, bc1, solver_parameters={"linear_solver": "mumps"})
    solve(aa_bc == L_bc, bc_func_2, bc2, solver_parameters={"linear_solver": "mumps"})
    with XDMFFile("output/bc_func_nr_{:d}.xdmf".format(ii)) as xf:
        xf.write_checkpoint(bc_func_1, bc_func_1.name())
    with XDMFFile("output/bc_func_new_nr_{:d}.xdmf".format(ii)) as xf:
        xf.write_checkpoint(bc_func_2, bc_func_2.name())
    vv = bc_func_2.vector()
    vv -= bc_func_1.vector()
    with XDMFFile("output/bc_func_diff_nr_{:d}.xdmf".format(ii)) as xf:
        xf.write_checkpoint(bc_func_2, bc_func_2.name())

os.system("rm output/*")

for ii, (bc1, bc2) in enumerate(zip(bcs, bcs_new)):
    test_bc(bc1, bc2, ii)

# print(mesh.bounding_box_tree().compute_closest_entity(Point(0.436577, 0.252058, 0)))
# print(mesh.bounding_box_tree().compute_entity_collisions(Point(0.436577, 0.252058, 0)))
# print(get_subdomains_of_point(np.array([0.436577, 0.252058, 0])))

pp2 = [0.915921, 0.399612, 0]

print(mesh.bounding_box_tree().compute_closest_entity(Point(*pp2)))
print(mesh.bounding_box_tree().compute_entity_collisions(Point(*pp2)))
print(get_subdomains_of_point(np.array(pp2)))

print(vertical_symmetry_boundary_bodies_new(pp2, False))