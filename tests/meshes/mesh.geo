r = 0.05;
//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {1.0, 0, 0, 1.0};
//+
Point(3) = {20.0, 0, 0, 1.0};
//+
Point(5) = {0, 0, 20, 1.0};
//+
Point(6) = {0, 0, 1, 1.0};
//+
Point(7) = {1-r, 0, 1-r, 1.0};
//
Point(8) = {0, 0, 1.05, 1.0};
Point(9) = {1-r, 0, 1.05-r/2, 1.0};
//+
Point(10) = {1, 0, 1-r, 1.0};
Point(11) = {1-r, 0, 1, 1.0};
//
Point(12) = {1-r-r/2, 0, 1.05-r/2, 1.0};
Point(13) = {1-r-r/2, 0, 1.05, 1.0};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 10};
//+
Line(3) = {11, 6};
//+
Line(4) = {6, 1};
//+
Line(5) = {2, 3};
//+
Circle(6) = {3, 1, 5};
//+
Line(8) = {5, 8};
//
Line(9) = {8, 6};
//
Circle(10) = {10, 7, 11};
//
Line(11) = {13, 8};
Circle(13) = {9, 12, 13};
Line(12) = {11, 9};
//+
Line Loop(9) = {8, -11, -13, -12, -10, -2, 5, 6};
//+
Plane Surface(10) = {9};
//+
Line Loop(11) = {4, 1, 10, 2, 3};
//+
Plane Surface(12) = {11};
//+
Line Loop(13) = {12, 13, 11, 9, -3};
//+
Plane Surface(14) = {13};
//+

// Rotations

ex0[] = Extrude {{0, 0, 1}, {0, 0, 0}, Pi/6} {
  Surface{10};
};

ex1[] = Extrude {{0, 0, 1}, {0, 0, 0}, Pi/6} {
  Surface{12};
};

ex2[] = Extrude {{0, 0, 1}, {0, 0, 0}, Pi/6} {
  Surface{14};
};


Transfinite Line {5, -8, 22 } = 20 Using Progression 1.12;
Transfinite Line {6, 23} = 11 Using Progression 1;
Transfinite Line {-1, -54, 4} = 8 Using Progression 1.2;
Transfinite Line {-2, 21, 3, 57} = 15 Using Progression 1.08;
Transfinite Line {11, -17} = 12 Using Progression 1.08;
Transfinite Line {10, 20} = 5 Using Progression 1;
Transfinite Line {12, 19} = 3 Using Progression 1;
Transfinite Line {13, 18} = 4 Using Progression 1;

Transfinite Line {26, 30, 34, 38, 42} = 8 Using Progression 1;
Transfinite Line {46} = 8 Using Progression 1;


Physical Volume(0) = {ex0[1]};
Physical Volume(1) = {ex1[1]};
Physical Volume(2) = {ex2[1]};

//
// free space
// Physical Surface(0) = {10};
//
// specimen
// Physical Surface(1) = {12};
//
// electrode
// Physical Surface(2) = {14};
//
// horizontal axis
// Physical Line(10) = {1, 5};
//
// vertical axis
// Physical Line(11) = {4, 8};
//
// right free space boundary
// Physical Line(12) = {6};
//
// top free space boundary
// Physical Line(13) = {7};
//
// right specimen boundary
// Physical Line(14) = {2};
//
// top specimen boundary
// Physical Line(15) = {3};
