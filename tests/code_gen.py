
import os
os.system('mkdir -p ./dijitso_dir')

import dolfin

# does not work
dolfin.parameters['form_compiler']['cache_dir'] = os.path.abspath('./dijitso_dir')
dolfin.parameters['form_compiler']['output_dir'] = os.path.abspath('./dijitso_dir')

# only this way...
os.putenv("DIJITSO_CACHE_DIR", "./dijitso_dir")

import ffc
import ufl

mesh = dolfin.IntervalMesh(1, 0, 1)
celmt = dolfin.FiniteElement("R", mesh.ufl_cell(), degree=0)
elmt = dolfin.TensorElement("R", mesh.ufl_cell(), degree=0, shape=(3,3))
V = dolfin.FunctionSpace(mesh, elmt)
F = dolfin.Function(V)

Vc = dolfin.FunctionSpace(mesh, celmt)
mu = dolfin.Function(Vc)

psi = mu/2 * dolfin.inner(F, F)
#
Pi = psi * dolfin.dx
PK1 = dolfin.derivative(Pi, F)

# use ffc.jitcompiler.jit(Pi, )
# use ffc.jitcompiler.jit(Pi, ) to generate code
# maybe modify dijitso.jit.jit(...) or directly dijitso.build.build_shared_library() where the compilation happens

Pi_form = dolfin.jit.jit.ffc_jit(Pi)
PK1_form = dolfin.jit.jit.ffc_jit(PK1)

print(Pi_form[1]._name)
print(PK1_form[1]._name)
