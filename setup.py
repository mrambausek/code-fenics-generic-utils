from setuptools import setup

setup(
    name='Various Utils for FEniCS',
    version='0.0.1',
    packages=['fenicsutils'],
    install_requires=['fenics-ufl', 'fenics-ffc', 'fenics-dolfin'],
    url='',
    license='GPL v3',
    author='Matthias Rambausek',
    author_email='matthias.rambausek@gmail.com',
    description='A lib providing some useful tools for FEniCS.'
)
